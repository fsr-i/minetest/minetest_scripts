KIF Minetest Server
===================

A role for setting up a minetest server for the Konferenz der Informatikfachschaften

Requirements
------------

None (?)

Role Variables
--------------

TODO: complete documentation

Most important variables:

* `use_production_secrets`: bool, default: `false`. Whether to use secrets for production from the vault file. Requires you to know and supply the password.
* `minetest_version`: string, default: `5.4.1`. Which minetest version to use. Refers to the commit, tag or branch of the official git repository where minetest is compiled from.
* `minetest_game_version`: string, default: `5.4.1`. Which minetest game version to use. Refers to the commit, tag or branch of the official git repository.
* `mods_version`: string, default: `master`. Refers to the commit, tag or branch of the repository holding the mods.
* `mods_git_repo`: string, default: `'https://gitlab.fachschaften.org/minetest/minetest_mods.git'`. URL of the repository holding the mods that should be used.
* `update_mods_cron_job`: bool, default: `true`. Whether to create a cron job to update mods automatically each day. Refers cron to `update.sh` which must be present at the root of the mods repository.
* `minetest_base_dir`: string, default: `/var/lib/minetest`. Path of the home directory of the minetest user, where all the minetest things should be placed.
* `server_auth_backend`: string, default: `sqlite3`. Please refer to the minetest documentation.
* `server_player_backend`: string, default: `postgresql`. Please refer to the minetest documentation.
* `server_backend`: string, default: `postgresql`. Please refer to the minetest documentation.
* `psql_host`: string, default: `127.0.0.1`. Host for connecting to PostgreSQL, if that backend is to be used.
* `psql_user`: string, default: `minetest`. User for connecting to PostgreSQL, if that backend is to be used.
* `psql_password`: string, default: `CHANGE_TO_PROPER_DB_PASSWORD`. Password for connecting to PostgreSQL, if that backend is to be used. Please, please always have this be something different than the default.
* `psql_db`: string, default: `minetest-db`. Database name for connecting to PostgreSQL, if that backend is to be used.
* `minetest_seed`: string or number, default: `3374822340961073250`. Seed when generating the minetest world.
* `server_address`: string, default: `minetest.kif.rocks`. Address to expect the server to be accessible as.
* `server_announce`: bool, default: `true`. Whether to announce the server status to a public server list. Please refer to the minetest documentation.
* `server_serverlist_url`: string, default: `servers.minetest.net`. URL of the server list to announce the server status to, if enabled. Please refer to the minetest documentation.
* `server_port`: number, default: `30000`. Port to open the minetest server on.
* `server_default_password`: string, default: `CHANGE_TO_PROPER_PLAYER_PASSWORD`. Default password for new clients connecting to the server. Please, please always have this be something different than the default.
* `server_default_privs`: list, default: `['interact', 'shout', 'railway_operator', 'interlocking', 'itm', 'train_operator', 'ui_full', 'travelnet_attach', 'track_builder', 'home', 'atlatc']`. Priviledges to give new players. Please refer to the official minetest documentation as well as the documentation of the mods registering the priviledges.
* `server_secure_http_mods`: list, default: `['mapserver']`. Please refer to the official minetest documentation.
* `mapserver_url`: string, default: `http://127.0.0.1:{{ mapserver_port }}`. URL over which the mapserver companion mod to should exchange in-game data with the mapserver.
* `mapserver_key`: string, default: `CHANGE_TO_PROPER_PASSWORD`. Secret for exchanging in-game data from the companion mod with the mapserver. Please, please always have this be something different than the default.
* `load_mods`: list, default omitted for brevity. Mods to enable.
* `disabled_mods`: list, default omitted for brevity. Mods to explicitly disable. Not needed for the configuration, but for idempotence as the minetest server adds the present but not enabled mods to the config on startup.
* *you can use* `enable_mods` *and* `disable_mods` *to enable/disable mods in addition to the default. Mods that appear in both lists will be disabled.*
* ...

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - role: kif_minetest_server
          server_player_backend: sqlite3
          server_backend: sqlite3
          server_announce: false
          server_default_password: CHANGE_TO_PROPER_PASSWORD

License
-------

MIT

Author Information
------------------

- https://minetest.kif.rocks/
- https://gitlab.fachschaften.org/minetest/
